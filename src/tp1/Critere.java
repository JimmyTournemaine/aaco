/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

/**
 * Condition to accept a node
 * 
 * @author Jimmy Tournemaine
 */
public interface Critere {
	
	boolean pass(Node n);
}
