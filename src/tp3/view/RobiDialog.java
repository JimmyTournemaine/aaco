/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;

/**
 * @author Jimmy Tournemaine
 */
public class RobiDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = -8148561448386138327L;
	private final JPanel contentPanel = new JPanel();
	private BufferedImage background;

	/**
	 * Create the dialog.
	 */
	public RobiDialog(String message) {
		setTitle("Robi is speaking...");
		try {
			background = ImageIO.read(new File("assets/background.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(207, 20, 220, 135);
		contentPanel.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		JTextArea textArea = new JTextArea(message);
		textArea.setTabSize(4);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		textArea.setFont(new Font("Courier New", Font.PLAIN, 13));
		textArea.setEditable(false);
		textArea.setBackground(null);
		panel.add(textArea, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(this);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}
	
	public void paint(Graphics g){
		super.paint(g);
		if(background != null){
			g.drawImage(background, 0, 20, this);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		dispose();
	}
}
