/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

/**
 * @author Jimmy Tournemaine
 */
abstract class Command {
	abstract public void run(Robi robi, String... args) throws BadCommandCall;
}
