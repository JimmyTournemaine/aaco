/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Jimmy Tournemaine
 */
public class Directory extends Node {

	private ArrayList<Node> children;

	public Directory(String name) {
		super(name);
		this.children = new ArrayList<Node>();
	}

	public ArrayList<Node> findBy(Critere crit) {
		ArrayList<Node> found = super.findBy(crit);
		Iterator<Node> it = this.children.iterator();
		while (it.hasNext()) {
			Node n = it.next();
			found.addAll(n.findBy(crit));
		}
		return found;
	}

	protected void traverse(NodeAction action, int depth) {

		action.doOn(this, depth);

		ArrayList<Node> kids = new ArrayList<Node>(children);
		ActionMode mode = action.getMode();

		if (mode == ActionMode.ALPHABETICAL) {
			kids.sort(new AlphabeticalComparator());
		} else if (mode == ActionMode.DIRECTORY_FIRST) {
			kids.sort(new DirectoryFirstComparator());
		}

		Iterator<Node> it = kids.iterator();
		while (it.hasNext()) {
			it.next().traverse(action, depth+1);
		}
	}

	public void addChild(Node node) {
		node.setParent(this);
		this.children.add(node);
	}

	public void addChildren(Node... children) {
		for (final Node child : children) {
			this.addChild(child);
		}
	}

	public boolean hasChild(Node node) {
		return this.children.contains(node);
	}

	public void removeChild(Node node) {
		node.setParent(null);
		this.children.remove(node);
	}

}
