/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.view;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;

import tp3.model.Robi;

/**
 * @author Jimmy Tournemaine
 */
public class RobiCanvas extends Canvas implements Observer {

	private static final long serialVersionUID = -88093900774231078L;
	private Robi robi;
	private BufferedImage image;

	/**
	 * Get the model and load the robot image
	 * @param model
	 */
	RobiCanvas(Robi robi) {
		this.robi = robi;
		try {
			image = ImageIO.read(new File(Assets.getRobot()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Draw on the canvas
	 */
	public void paint(Graphics g) {
		
		this.setMinimumSize(new Dimension(image.getWidth()*robi.getMaxX(), image.getHeight()*robi.getMaxY()));
		
		/* Computing */
		int nbCellPerRow = robi.getMaxY();
		int nbCellPerCol = robi.getMaxX();
		int cellHeight = (int) Math.floor(this.getHeight() / nbCellPerRow);
		int cellWidth = (int) Math.floor(this.getWidth() / nbCellPerCol);
		int right = cellWidth * nbCellPerCol;
		int bot = cellHeight * nbCellPerRow;

		/* Draw Lines */
		for (int i = 0; i <= nbCellPerCol; i++) {
			int y = i * cellHeight;
			g.drawLine(0, y, right, y);
		}
		for (int i = 0; i <= nbCellPerRow; i++) {
			int x = i * cellWidth;
			g.drawLine(x, 0, x, bot);
		}

		/* Draw image at the position of Robi */
		int imageX = robi.getPosition().x() * cellWidth;
		int imageY = robi.getPosition().y() * cellHeight;
		g.drawImage(image, imageX+cellWidth/2-image.getWidth()/2, imageY+cellHeight/2-image.getHeight()/2, this);
	}

	@Override
	public void update(Observable o, Object arg) {
		this.repaint();
	}
}
