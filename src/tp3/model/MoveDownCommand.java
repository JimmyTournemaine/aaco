/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

/**
 * @author Jimmy Tournemaine
 */
class MoveDownCommand extends Command {

	@Override
	public void run(Robi robi, String... args) throws BadCommandCall {
		Coordinates pos = robi.getPosition();
		if(pos.y() >= robi.getMaxY()-1){
			throw new BadCommandCall("Robi cannot move down.");
		}
		robi.setPosition(new Coordinates(pos.x(), pos.y()+1));
	}

}
