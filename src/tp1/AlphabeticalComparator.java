/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

import java.util.Comparator;

/**
 * @author Jimmy Tournemaine
 */
public class AlphabeticalComparator implements Comparator<Node> {

	@Override
	public int compare(Node o1, Node o2) {
		return o1.getName().compareToIgnoreCase(o2.getName());
	}
}
