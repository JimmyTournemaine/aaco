/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineEvent.Type;

/**
 * @author Jimmy Tournemaine
 */
class BeepCommand extends Command implements LineListener {
	
	Clip clip;
	
	BeepCommand(){
		try {
			clip = AudioSystem.getClip();
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File("assets/beep.wav"));
			clip.open(inputStream);
			clip.addLineListener(this);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	@Override
	public void run(Robi robi, String... args) {
		this.playSound();
	}

	private synchronized void playSound() {
		new Thread(new Runnable() {
			public void run() {
				clip.start();
			}
		}).start();
	}

	@Override
	public void update(LineEvent event) {
		if(event.getType() == Type.STOP){
			clip.setMicrosecondPosition(0);
		}
	}

}
