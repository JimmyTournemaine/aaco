/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp2;

import javax.management.Query;
import junit.framework.TestCase;
import tp1.*;

/**
 * @author Jimmy Tournemaine
 */
public class HashDictTest extends TestCase {

	public void test() {

		Map dico = new HashDict();
		Object o1 = new String("OK");
		Object o2 = new Integer(8);
		Object o3 = new Query();
		Object o4 = new Directory("/");
		Object o5 = "test";
		Object o6 = new File("Mon fichier");

		assertTrue(dico.isEmpty());
		assertFalse(dico.containsKey(o1));

		dico.put(o1, o2);
		dico.put(o3, o4);
		dico.put(o5, o6);

		assertFalse(dico.isEmpty());
		assertTrue(dico.size() == 3);
		assertTrue(dico.containsKey(o1));
		assertTrue(dico.containsKey(o3));
		assertTrue(dico.containsKey(o5));
		assertTrue(dico.containsValue(o2));
		assertTrue(dico.containsValue(o4));
		assertTrue(dico.containsValue(o6));
		assertTrue(dico.size() == 3);

		assertTrue(dico.get(o1) == o2);
		assertTrue(dico.get(o3) == o4);
		assertTrue(dico.get(o5) == o6);

		assertTrue(dico.remove(o1) == o2);
		assertTrue(dico.size() == 2);
		assertFalse(dico.containsKey(o1));
		assertFalse(dico.containsValue(o2));
	}
}
