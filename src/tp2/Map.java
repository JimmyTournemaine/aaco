/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp2;

/**
 * @author Jimmy Tournemaine
 */
public interface Map {

    Object put(Object key, Object value);
    Object get(Object key);
    Object remove(Object key);
    boolean containsKey(Object key);
    boolean containsValue(Object value);
    int size();
    boolean isEmpty();
    
    public interface Entry {
        Object getKey();
        Object getValue();
        Object setValue(Object value);
    }
}
