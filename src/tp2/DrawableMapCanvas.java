/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp2;

import java.awt.Canvas;
import java.awt.Graphics;

import javax.management.Query;

import tp1.Directory;
import tp1.File;

/**
 * @author Jimmy Tournemaine
 */
public class DrawableMapCanvas extends Canvas {

	private boolean resized;
	private boolean displayNull;
	private DrawableHashDict map;
	private static final long serialVersionUID = -5709828742968034930L;
	
	public DrawableMapCanvas(){
		createMap();
		resized = false;
		displayNull = false;
	}
	
	public void setDisplayNull(boolean printNull) {
		displayNull = printNull;
		resized = false;
		this.repaint();
	}

	public void paint(Graphics g){
		this.setPreferredSize(map.paint(g, displayNull));
		if(resized)
			return;
		
		this.revalidate();
		resized = true;
	}
	
	private void createMap() {
		DrawableHashDict dico = new DrawableHashDict();
		Object o1 = new String("OK");
		Object o2 = new Integer(8);
		Object o3 = new Query();
		Object o4 = new Directory("/");
		Object o5 = "test";
		Object o6 = new File("Mon fichier");
		dico.put(o1, o2);
		dico.put(o3, o4);
		dico.put(o5, o6);
		dico.put(o1, o4);
		dico.put(o1, o5);
		dico.put(o5, o3);
		this.map = dico;
	}

	public void addToDict(String text, String text2) {
		map.put(text, text2);
		resized = false;
		repaint();
	}

}
