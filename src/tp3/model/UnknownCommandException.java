/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

/**
 * @author Jimmy Tournemaine
 */
public class UnknownCommandException extends Exception {

	public UnknownCommandException(String string) {
		super(string);
	}

	private static final long serialVersionUID = -1131605846248813689L;

}
