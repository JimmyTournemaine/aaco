/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

/**
 * @author Jimmy Tournemaine
 */
public class CritereAny implements Critere {

	@Override
	public boolean pass(Node n) {
		return true;
	}

}
