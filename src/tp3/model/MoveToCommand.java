/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

class MoveToCommand extends Command {

	@Override
	public void run(Robi robi, String... args) throws BadCommandCall {
		int x = 0;
		int y = 0;
		try {
			if(args.length != 3){
				throw new NumberFormatException();
			}
			x = Integer.parseInt(args[1]);
			y = Integer.parseInt(args[2]);
		} catch (NumberFormatException e) {
			throw new BadCommandCall("Usage : "+args[0]+" x y");
		}
		
		if(x < 0 || x > robi.getMaxX()){
			throw new BadCommandCall("x needs to be in [0,"+robi.getMaxX()+"]");
		}
		if(y < 0 || y > robi.getMaxX()){
			throw new BadCommandCall("x needs to be in [0,"+robi.getMaxY()+"]");
		}
		robi.setPosition(new Coordinates(x, y));
	}

}
