/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp2;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Iterator;

/**
 * @author Jimmy Tournemaine
 */
public class DrawableHashDict extends HashDict {

	private final int width = 50;
	private final int height = 20;
	private final int offset = 20;
	private final int padding = 2;

	/**
	 * Pain each hash cells (not null)
	 */
	public Dimension paint(Graphics graph, boolean displayNull) {
		Graphics2D g = (Graphics2D) graph;
		int j = 0, maxX=0, y=0;
		for (int i = 0; i < tab.length; i++) {
			int x = offset;
			y = j * height + offset;
			if (tab[i] == null) {
				if(displayNull){
					g.draw(new Rectangle(x, y, width, height));
					g.drawString("NULL", x + padding, y + 2 * height / 3);
					j++;
				}
				continue;
			}
			g.draw(new Rectangle(x, y, width, height)); // Draw hash cell
			x += width;
			Iterator<LinkedEntry> it = tab[i].iterator();
			while (it.hasNext()) {
				/* Draw an entry */
				LinkedEntry e = it.next();
				String str = e.toString();
				int cellWidth = g.getFontMetrics().stringWidth(str) + padding * 2;
				drawArrow(g, x, y + height / 2, x + width, y + height / 2);
				x += width;
				g.drawRect(x, y, cellWidth, height);
				g.drawString(e.toString(), x + padding, y + 2 * height / 3);
				x += cellWidth;
				if(x > maxX)
					maxX = x;
			}
			j++;
		}
		return new Dimension(maxX+offset, y+offset);
	}

	/**
	 * Draws an arrow on the given Graphics2D context
	 * 
	 * @param g
	 *            The Graphics2D context to draw on
	 * @param x
	 *            The x location of the "tail" of the arrow
	 * @param y
	 *            The y location of the "tail" of the arrow
	 * @param xx
	 *            The x location of the "head" of the arrow
	 * @param yy
	 *            The y location of the "head" of the arrow
	 */
	private void drawArrow(Graphics2D g, int x, int y, int xx, int yy) {
		float arrowWidth = 10.0f;
		float theta = 0.423f;
		int[] xPoints = new int[3];
		int[] yPoints = new int[3];
		float[] vecLine = new float[2];
		float[] vecLeft = new float[2];
		float fLength;
		float th;
		float ta;
		float baseX, baseY;

		xPoints[0] = xx;
		yPoints[0] = yy;

		// build the line vector
		vecLine[0] = (float) xPoints[0] - x;
		vecLine[1] = (float) yPoints[0] - y;

		// build the arrow base vector - normal to the line
		vecLeft[0] = -vecLine[1];
		vecLeft[1] = vecLine[0];

		// setup length parameters
		fLength = (float) Math.sqrt(vecLine[0] * vecLine[0] + vecLine[1] * vecLine[1]);
		th = arrowWidth / (2.0f * fLength);
		ta = arrowWidth / (2.0f * ((float) Math.tan(theta) / 2.0f) * fLength);

		// find the base of the arrow
		baseX = ((float) xPoints[0] - ta * vecLine[0]);
		baseY = ((float) yPoints[0] - ta * vecLine[1]);

		// build the points on the sides of the arrow
		xPoints[1] = (int) (baseX + th * vecLeft[0]);
		yPoints[1] = (int) (baseY + th * vecLeft[1]);
		xPoints[2] = (int) (baseX - th * vecLeft[0]);
		yPoints[2] = (int) (baseY - th * vecLeft[1]);

		g.drawLine(x, y, (int) baseX, (int) baseY);
		g.fillPolygon(xPoints, yPoints, 3);
	}
}
