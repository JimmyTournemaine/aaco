/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

import java.util.ArrayList;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * Test cases
 * 
 * @author Jimmy Tournemaine
 */
public class NodeTest extends TestCase {

	/**
	 * Test the files' names
	 */
	@Test
	public void testName() {
		Directory root = new Directory("");
		Directory usr = new Directory("usr");
		Directory etc = new Directory("etc");

		assertEquals("", root.getName());
		assertEquals("usr", usr.getName());
		assertEquals("etc", etc.getName());
	}

	/**
	 * Test the node-adding in a directory
	 */
	@Test
	public void testAddDirectoryChild() {
		Directory root = new Directory("");
		Directory usr = new Directory("usr");
		Directory etc = new Directory("etc");
		Directory bin = new Directory("bin");
		Directory dev = new Directory("dev");

		/* Ajout simple */
		assertFalse(root.hasChild(usr));
		root.addChild(usr);
		assertTrue(root.hasChild(usr));

		/* Ajout multiple */
		assertFalse(root.hasChild(etc));
		root.addChildren(etc, bin, dev);
		assertTrue(root.hasChild(etc) && root.hasChild(bin) && root.hasChild(dev));
	}

	/**
	 * Test the research feature
	 */
	@Test
	public void testResearch() {
		Directory root = new Directory("");
		Directory usr = new Directory("usr");
		Directory etc = new Directory("etc");
		Directory priv = new Directory("private");
		File f1 = new File("fichier1");
		File f2 = new File("fichier2");
		File f3 = new File("fichier1");
		
		/*
		 * root
		 * -- usr
		 *    -- f1 "fichier1"
		 *    -- f2 "fichier2"
		 * -- etc
		 *    -- private
		 *       -- f3 "fichier1"
		 */
		root.addChildren(usr, etc);
		usr.addChildren(f1, f2);
		etc.addChild(priv);
		priv.addChild(f3);

		ArrayList<Node> found = root.findBy(new CritereNameEquals("fichier1"));
		assertTrue(found.size() == 2 && found.contains(f1) && found.contains(f3));
		
		found = root.findBy(new CritereNameEquals("fichier2"));
		assertTrue(found.size() == 1 && found.contains(f2));
		
		found = root.findBy(new CritereNameEquals("usr"));
		assertTrue(found.size() == 1 && found.contains(usr));
		
		found = root.findBy(new CritereNameEquals("un_truc_indexistant"));
		assertTrue(found.isEmpty());
		
		found = root.findBy(new CritereAny());
		assertTrue(found.size() == 7);
	}
	
	@Test
	public void testTraverse() {
		Directory root = new Directory("/");
		Directory usr = new Directory("usr");
		Directory etc = new Directory("etc");
		Directory priv = new Directory("private");
		File f1 = new File("fichier1");
		File f2 = new File("fichier2");
		File f3 = new File("fichier1");
		
		/*
		 * root
		 * -- usr
		 *    -- f1 "fichier1"
		 *    -- f2 "fichier2"
		 * -- etc
		 *    -- private
		 *       -- f3 "fichier1"
		 */
		root.addChildren(usr, etc);
		usr.addChildren(f1, f2);
		etc.addChild(priv);
		priv.addChild(f3);

		System.out.println("Alphabetical Mode :");
		root.traverse(new DisplayHierarchyAction(ActionMode.ALPHABETICAL, System.out));
		
		System.out.println("Directory First Mode :");
		root.traverse(new DisplayHierarchyAction(ActionMode.DIRECTORY_FIRST, System.out));

		System.out.println("Directory Only Mode :");
		root.traverse(new DisplayHierarchyAction(ActionMode.DIRECTORY_ONLY, System.out));
	}
	
}
