/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

/**
 * @author Jimmy Tournemaine
 */
abstract public class NodeAction implements Action {
	
	protected ActionMode mode;
	
	NodeAction(ActionMode mode) {
		this.mode = mode;
	}

	public ActionMode getMode() {
		return mode;
	}
}
