/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

import java.io.PrintStream;

/**
 * @author Jimmy Tournemaine
 */
public class DisplayHierarchyAction extends NodeAction {

	private PrintStream stream;
	
	DisplayHierarchyAction(ActionMode mode, PrintStream stream) {
		super(mode);
		this.stream = stream;
	}

	@Override
	public void doOn(Node n, int depth) {
		String str = "";
		for(int i=0 ; i<depth ; i++){
			str += '\t';
		}
		str += n.name;

		stream.println(str);
	}

}
