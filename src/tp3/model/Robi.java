/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

import java.util.HashMap;
import java.util.Observable;
import java.util.Set;

/**
 * @author Jimmy Tournemaine
 */
public class Robi extends Observable {
	static HashMap<String, Command> COMMANDS;
	private final int maxX = 4;
	private final int maxY = 5;
	private Coordinates position;

	static {
		COMMANDS = new HashMap<String, Command>();
		COMMANDS.put("left", new MoveLeftCommand());
		COMMANDS.put("up", new MoveUpCommand());
		COMMANDS.put("right", new MoveRightCommand());
		COMMANDS.put("down", new MoveDownCommand());
		COMMANDS.put("beep", new BeepCommand());
		Command say = new SayCommand();
		COMMANDS.put("say", say);
		COMMANDS.put("says", say);
		COMMANDS.put("tell", say);
		COMMANDS.put("speak", say);
		COMMANDS.put("moveTo", new MoveToCommand());
	}
	
	static public Set<String> getCommands(){
		return COMMANDS.keySet();
	}
	
	public Robi(){
		position = new Coordinates();
	}
	
	public void run(String[] args) throws UnknownCommandException, BadCommandCall  {
		Command com = COMMANDS.get(args[0]);
    	if(com == null)
    		throw new UnknownCommandException(args[0] + " : unknown command.");
    	com.run(this, args);
    	this.notifyObservers();
	}

	public Coordinates getPosition(){
		return this.position;
	}
	
	public void setPosition(Coordinates pos){
		position = pos;
		setChanged();
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMaxY() {
		return maxY;
	}
}
