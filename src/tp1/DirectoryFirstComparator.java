/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

import java.util.Comparator;

/**
 * @author Jimmy Tournemaine
 */
public class DirectoryFirstComparator implements Comparator<Node> {

	@Override
	public int compare(Node o1, Node o2) {

		boolean isDir1 = o1 instanceof Directory;
		boolean isDir2 = o2 instanceof Directory;

		if (isDir1 == isDir2) {
			return alphaCompare(o1, o2);
		}

		if (!isDir2) {
			return -1;
		}

		return 1;
	}

	private int alphaCompare(Node o1, Node o2) {
		return o1.getName().compareToIgnoreCase(o2.getName());
	}

}
