/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

/**
 * @author Jimmy Tournemaine
 */
public enum ActionMode {
	DIRECTORY_ONLY,
	DIRECTORY_FIRST,
	ALPHABETICAL
}
