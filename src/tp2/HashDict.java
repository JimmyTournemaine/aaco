/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp2;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author Jimmy Tournemaine
 */
public class HashDict implements Map {

	protected LinkedList<LinkedEntry>[] tab;
	protected final int block = 128;

	@SuppressWarnings("unchecked")
	public HashDict() {
		tab = new LinkedList[block];
	}

	@Override
	public Object put(Object key, Object value) {
		int hash = hashValue(key);
		System.out.println(hash);
		LinkedList<LinkedEntry> l = tab[hash];
		if(l == null){
			l = new LinkedList<LinkedEntry>();
		}
		l.add(new LinkedEntry(key, value));
		tab[hash] = l;
		
		return value;
	}

	@Override
	public Object get(Object key) {
		Iterator<LinkedEntry> it = tab[hashValue(key)].iterator();
		LinkedEntry cur;
		while (it.hasNext()) {
			cur = (LinkedEntry) it.next();
			if (cur.getKey().equals(key)) {
				return cur.getValue();
			}
		}

		return null;
	}

	@Override
	public Object remove(Object key) {
		LinkedList<LinkedEntry> l = tab[hashValue(key)];
		Iterator<LinkedEntry> it = l.iterator();
		LinkedEntry cur;
		
		while(it.hasNext()){
			cur = (LinkedEntry) it.next();
			if(cur.getKey() == key){
				l.removeFirstOccurrence(cur);
				return cur.getValue();
			}
		}
		return null;
	}

	@Override
	public boolean containsKey(Object key) {
		Iterator<LinkedEntry> it2;
		
		for(int i=0 ; i<tab.length; i++){
			if(tab[i] == null)
				continue;
			it2 = tab[i].iterator();
			while(it2.hasNext()){
				if(it2.next().getKey().equals(key)){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean containsValue(Object value) {
		Iterator<LinkedEntry> it2;
		for(int i=0 ; i<tab.length; i++){
			if(tab[i] == null)
				continue;
			it2 = tab[i].iterator();
			while(it2.hasNext()){
				if(it2.next().getValue().equals(value)){
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public int size() {
		int sum = 0;
		for(int i=0 ; i<tab.length; i++){
			if(tab[i] != null){
				sum += tab[i].size();
			}
		}
		return sum;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	class LinkedEntry implements Entry {

		private Object key;
		private Object value;

		public LinkedEntry(Object key2, Object value2) {
			this.key = key2;
			this.value = value2;
		}
		
		public String toString(){
			return "("+key.toString()+","+value.toString()+")";
		}

		@Override
		public Object getKey() {
			return key;
		}

		@Override
		public Object getValue() {
			return value;
		}

		@Override
		public Object setValue(Object value) {
			this.value = value;
			return value;
		}
	}

	private int hashValue(Object o) {
		return Math.abs(o.hashCode()) % tab.length;
	}
}
