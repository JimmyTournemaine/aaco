/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

/**
 * @author Jimmy Tournemaine
 */
public class CritereNameEquals implements Critere {

	protected String name;
	
	CritereNameEquals(String name) {
		this.name = name;
	}
	
	@Override
	public boolean pass(Node n) {
		return n.getName().equals(this.name);
	}
}
