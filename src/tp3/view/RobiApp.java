/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tp3.model.BadCommandCall;
import tp3.model.Robi;
import tp3.model.UnknownCommandException;

import java.awt.Dimension;

/**
 * @author Jimmy Tournemaine
 */
public class RobiApp extends JFrame implements ActionListener {

	private static final long serialVersionUID = 473109995812863880L;
	private JPanel contentPane;
	private JTextField field;
	private Robi model;
	private RobiCanvas canvas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RobiApp frame = new RobiApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RobiApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 350);
		setMinimumSize(new Dimension(400, 350));
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		model = new Robi();
		
		field = new JTextField(20);
		JButton run = new JButton("Run !");
		run.addActionListener(this);
		JButton help = new JButton(new ImageIcon(Assets.getHelpIcon()));
		help.addActionListener(this);
		JPanel pan = new JPanel();
		pan.setLayout(new FlowLayout());
		pan.add(field);
		pan.add(run);
		pan.add(help);
		
		canvas = new RobiCanvas(model);
		model.addObserver(canvas);
		contentPane.add(canvas, BorderLayout.CENTER);
		contentPane.add(pan, BorderLayout.NORTH);
		contentPane.getRootPane().setDefaultButton(run);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = ((JButton) e.getSource());
		if(b.getIcon() == null){
			try {
				String str = field.getText();
				model.run(str.split(" "));
			} catch(BadCommandCall | UnknownCommandException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			}
		} else {
			JOptionPane.showMessageDialog(this, Robi.getCommands().toString());
		}
	}
}
