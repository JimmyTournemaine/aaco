/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

/**
 * @author Jimmy Tournemaine
 */
class MoveUpCommand extends Command {

	@Override
	public void run(Robi robi, String... args) throws BadCommandCall {
		Coordinates pos = robi.getPosition();
		if(pos.y() <= 0){
			throw new BadCommandCall("Robi cannot move up.");
		}
		robi.setPosition(new Coordinates(pos.x(), pos.y()-1));
	}

}
