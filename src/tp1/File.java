/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

/**
 * @author Jimmy Tournemaine
 */
public class File extends Node {

	public File(String name) {
		super(name);
	}
	
	protected void traverse(NodeAction action, int depth){
		if(action.getMode() != ActionMode.DIRECTORY_ONLY){
			action.doOn(this, depth);
		}
	}

}
