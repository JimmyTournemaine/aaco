/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

import tp3.view.RobiDialog;

/**
 * @author Jimmy Tournemaine
 */
class SayCommand extends Command {
	
	@Override
	public void run(Robi robi, String... args) {
		String str = "";
		for(int i=1;i<args.length;i++){
			str += " "+ args[i];
		}
		
		if(!str.isEmpty())
			new RobiDialog(str);
	}

}
