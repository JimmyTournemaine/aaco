/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

/**
 * @author Jimmy Tournemaine
 */
class MoveRightCommand extends Command {
	
	@Override
	public void run(Robi robi, String... args) throws BadCommandCall {
		Coordinates pos = robi.getPosition();
		if (pos.x() >= robi.getMaxX() - 1) {
			throw new BadCommandCall("Robi cannot move to its right.");
		}
		robi.setPosition(new Coordinates(pos.x()+1, pos.y()));
	}

}
