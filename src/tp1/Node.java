/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp1;

import java.util.ArrayList;

/**
 * A node in the hierarchy
 * 
 * @author Jimmy Tournemaine
 */
public abstract class Node {
	protected String name;
	protected Node parent;

	Node(String name) {
		this.name = name;
	}
	
	public String toString(){
		return new String(this.name);
	}

	public ArrayList<Node> findBy(Critere crit) {
		ArrayList<Node> found = new ArrayList<Node>();
		if (crit.pass(this))
			found.add(this);
		return found;
	}
	
	public void traverse(NodeAction action){
		traverse(action, 0);
	}
	
	abstract protected void traverse(NodeAction action, int depth);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

}
