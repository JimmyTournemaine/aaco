/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.view;

/**
 * @author Jimmy Tournemaine
 */
public class Assets {
	
	static final String baseFolder = "assets/";
	
	static String getRobot() {
		return baseFolder+"robot.png";
	}
	
	static String getRobotBadMove() {
		return baseFolder+"robot-aie.png";
	}
	
	static String getHelpIcon(){
		return baseFolder+"help-icon.png";
	}
	
	
}
