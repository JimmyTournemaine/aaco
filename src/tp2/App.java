/*
 * Created on 5 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp2;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * @author Jimmy Tournemaine
 */
public class App extends JFrame {

	private static final long serialVersionUID = 1700648330141172238L;
	private JPanel contentPane;

	static public void main(String argv[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App frame = new App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public App() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		ScrollPane scroll = new ScrollPane();
		DrawableMapCanvas canvas = new DrawableMapCanvas();
		scroll.add(canvas, ScrollPane.SCROLLBARS_AS_NEEDED);
		contentPane.add(scroll, BorderLayout.CENTER);
		contentPane.add(radios(canvas), BorderLayout.NORTH);
	}

	private Component radios(DrawableMapCanvas canvas) {
		JPanel pan = new JPanel();
		JCheckBox box = new JCheckBox("Display NULL");
		box.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox b = (JCheckBox) e.getSource();
		        canvas.setDisplayNull(b.isSelected());
			}
		});
		JTextField key = new JTextField();
		JTextField value = new JTextField();
		JButton add = new JButton("Add");
		add.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				canvas.addToDict(key.getText(), value.getText());
			}
		});
		
		pan.add(box);
		pan.add(key);
		pan.add(value);
		pan.add(add);
		
		return pan;
	}
}
