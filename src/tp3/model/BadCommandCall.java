/*
 * Created on 10 oct. 2016 under the authority of Alain Plantec 
 * as part of practical work at the University of Western Brittany
 */
package tp3.model;

/**
 * @author Jimmy Tournemaine
 */
public class BadCommandCall extends Exception {
	
	public BadCommandCall(String message) {
		super(message);
	}

	private static final long serialVersionUID = -1329208260950978801L;

}
